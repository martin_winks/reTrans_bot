# latets update was edited in GitLab editor
import asyncio

from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor
# file with existing algo in it
from qwertuner import yzuken_keyboard, upper_yzuken
from qwertuner import qwerty_letters, upper_qwerty
import qwertuner

API_TOKEN = ''  # It won't work without token!

loop = asyncio.get_event_loop()

bot = Bot(token=API_TOKEN, loop=loop)
dp = Dispatcher(bot)


async def retrans_main(z: str()):
    output = str()
    checker_array = list(z)
    for i in checker_array:
        if str(i) in upper_qwerty or str(i) in qwerty_letters:
            if str(i) in upper_qwerty:
                inc = upper_qwerty.index(i)
                output += upper_yzuken[inc]
            elif str(i) in qwerty_letters:
                inc = qwerty_letters.index(i)
                output += yzuken_keyboard[inc]
            else:
                    output += str(i)
        elif str(i) in upper_yzuken or str(i) in yzuken_keyboard:
            if str(i) in upper_yzuken:
                inc = upper_yzuken.index(i)
                output += upper_qwerty[inc]
            elif str(i) in yzuken_keyboard:
                inc = yzuken_keyboard.index(i)
                output += qwerty_letters[inc]
            else:
                    output += str(i)
    return output


# piece of shiittiest code, I've ever coded
@dp.message_handler(commands=['rt'])
async def retranslate_it(message: types.Message):
    z = message
    if z.reply_to_message:
        if z.reply_to_message.text:
            button = types.InlineKeyboardButton(text='Share', switch_inline_query=str(z.reply_to_message.text))
            markup = types.InlineKeyboardMarkup(row_width=1).row(button)
            stdout = await retrans_main(z.reply_to_message.text)
            await z.reply_to_message.reply(f'{stdout}', reply_markup=markup)
    else:
        splited = str(z.text).split()
        if len(splited) >= 2:
            button = types.InlineKeyboardButton(text='Share', switch_inline_query=str(z.text).replace('/rt ', ''))
            markup = types.InlineKeyboardMarkup(row_width=1).row(button)
            std_void_out = await retrans_main(str(z.text).replace('/rt ', ''))
            await z.reply(f'{std_void_out}', reply_markup=markup)
# endpoint


@dp.message_handler(commands=['start', 'help'])
async def replier(message: types.Message):
    if str(message.from_user.language_code)[0:2] == 'ru':
        await message.reply(await qwertuner.rus_hello(message.from_user.first_name, True), parse_mode='Markdown')
    else:
        await message.reply(await qwertuner.rus_hello(message.from_user.first_name, False), parse_mode='Markdown')


@dp.inline_handler(func=lambda inline: len(inline.query) >= 1)
async def receive_inline_shit(inline: types.InlineQuery):
    items = []
    output_content = f"*{inline.query}*\n->\n"
    x_title = await retrans_main(inline.query)
    output_content += x_title
    input_content = types.InputTextMessageContent(message_text=output_content, parse_mode='Markdown')
    button = types.InlineKeyboardButton(text='Share', switch_inline_query=f'{str(inline.query)}')
    markup = types.InlineKeyboardMarkup(row_width=1).row(button)
    item = types.InlineQueryResultArticle(id=str(0), title=f"{x_title}...",
                                          input_message_content=input_content,
                                          reply_markup=markup)
    items.append(item)
    await bot.answer_inline_query(inline.id, results=items, cache_time=0)


@dp.message_handler(func=lambda message: message.chat.type == 'private')
async def is_text(message: types.Message):
    if not message.entities:
        print(message.content_type)
        std_void_out = await retrans_main(str(message.text))
        button = types.InlineKeyboardButton(text='Share', switch_inline_query=str(message.text))
        markup = types.InlineKeyboardMarkup(row_width=1).row(button)
        await message.reply(f'{std_void_out}', reply_markup=markup)


if __name__ == '__main__':
    executor.start_polling(dp, loop=loop, skip_updates=True)
