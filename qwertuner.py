# qwerty keyboard
qwerty_letters = list('qwertyuiop[]asdfghjkl;\'zxcvbnm,./ 1234567890-=\\')
upper_qwerty = list('QWERTYUIOP{}ASDFGHJKL:"ZXCVBNM<>? !@#$%^&*()_+|')
# йцукен keyboard
yzuken_keyboard = list('йцукенгшщзхъфывапролджэячсмитьбю. 1234567890-=\\')
upper_yzuken = list('ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ, !"№;%:?*()_+/')

# About


async def rus_hello(first_name, ru: bool) -> str:
    out = str()
    nickname = str(first_name).replace('_', '').replace('*', '').replace('`', '')
    if ru:
        out += f'Привет, {nickname}, я тот самый антитарабарщик бот. '\
              f'Антитарабарация происходит между йцукен и qwerty.'\
              f'\nИспользуй /rt ответ на сообщение или `/rt цукен`'
    elif not ru:
        out += f'Hey there, {nickname}. '\
               f'I retranslate cyrillic йцукен to qwerty or vice versa\n'\
               f'Reply /rt to message or type `/rt qwerty`'
    return out
